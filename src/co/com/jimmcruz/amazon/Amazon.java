package co.com.jimmcruz.amazon;

import java.util.Date;
import java.util.Scanner;

/**
 * @author jimmcruz
 *
 */
public class Amazon {

	/**
	 * Método que inicializa la aplicación
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Amazon amazon = new Amazon();
		amazon.showMenu();
	}

	/**
	 * Método constructor
	 */
	public Amazon() {
		super();
		System.out.println("Bienvenido a Amazon Viewer");
	}

	/**
	 * Método que muestra el menú
	 */
	public void showMenu() {
		AmazonFactory.getMenu(new Scanner(System.in));
	}

	/**
	 * Método que muestra las series
	 */
	public void showSeries() {
		int exit = 1;
		do {
			System.out.println();
			System.out.println("Series:");
			System.out.println();
		} while (exit != 0);
	}

	/**
	 * Método que muestra los capítulos
	 */
	public void showChapters() {
		int exit = 1;
		do {
			System.out.println();
			System.out.println("Chapters:");
			System.out.println();
		} while (exit != 0);
	}

	/**
	 * Método que muestra los libros
	 */
	public void showBooks() {
		int exit = 1;
		do {
			System.out.println();
			System.out.println("Books:");
			System.out.println();
		} while (exit != 0);
	}

	/**
	 * Método que muestra las revistas
	 */
	public void showMagazines() {
		int exit = 1;
		do {
			System.out.println();
			System.out.println("Magazines:");
			System.out.println();
		} while (exit != 0);
	}

	/**
	 * Método que genera un reporte de las peliculas vistas
	 */
	public void makeReport() {
		// TODO complementar
	}

	/**
	 * Método que genera un reporte de las peliculas vistas en la fecha suminstrada
	 * 
	 * @param date
	 */
	public void makeReport(Date date) {
		// TODO complementar
	}
	

}
