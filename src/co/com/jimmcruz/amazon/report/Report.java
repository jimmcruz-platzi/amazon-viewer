package co.com.jimmcruz.amazon.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * 
 * @author jimmcruz
 *
 */
public class Report {
	private String nameFile;
	private String title;
	private String content;

	/**
	 * 
	 * @param nameFile
	 */
	public Report(String nameFile) {
		this.nameFile = nameFile;
	}
	
	/**
	 * @param nameFile
	 * @param title
	 * @param content
	 */
	public Report(String nameFile, String title, String content) {
		super();
		this.nameFile = nameFile;
		this.title = title;
		this.content = content;
	}

	/**
	 * 
	 */
	public void makeReport() {
		try {
			if (title != null && nameFile != null && content != null) {
				File file = new File(nameFile);
				FileOutputStream fos = new FileOutputStream(file); // NOPMD by jimmcruz on 14/03/20, 11:41 a. m.
				OutputStreamWriter osw = new OutputStreamWriter(fos); // NOPMD by jimmcruz on 14/03/20, 11:41 a. m.
				BufferedWriter write = new BufferedWriter(osw); // NOPMD by jimmcruz on 14/03/20, 11:41 a. m.
				write.write(content);
				write.close();
				osw.close();
				fos.close();
			} else {
				System.out.println("Ingresa los datos del archivo");
			}
		} catch (IOException e) {
			System.out.println(e);
			// e.printStackTrace();
		}

	}

	/**
	 * @return the nameFile
	 */
	public String getNameFile() {
		return nameFile;
	}

	/**
	 * @param nameFile the nameFile to set
	 */
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Report [nameFile=");
		builder.append(nameFile);
		builder.append(", title=");
		builder.append(title);
		builder.append(", content=");
		builder.append(content);
		builder.append("]");
		return builder.toString();
	}

}
