package co.com.jimmcruz.amazon;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import co.com.jimmcruz.amazon.model.Movie;

/**
 * @author jimmcruz
 *
 */
public class MovieFunction extends AbstractFunction {
	
	private List<Movie> list;

	/**
	 * 
	 */
	protected MovieFunction(Scanner scanner) {
		super("Movies", scanner);
	}

	@Override
	public void show() {
		showSubTitle();
		while (true) {
			for (Movie val : makeList()) {
				val.showDataMin();
			}
			System.out.println("0. Regresar al menú anterior");
			String responsest = leerOpcion();
			if (isNumeric(responsest)) {
				int response = Integer.parseInt(responsest);
				if (response == 0)
					return;
				view(response);
			} else {
				System.out.println("Seleccione una opción valida");
			}
		}
	}

	/**
	 * 
	 * @param movie
	 */
	private void view(int pos) {
		if(pos <= list.size()) {
			view(list.get(pos - 1));
		} else {
			System.out.println("Seleccione una opción valida"); 
		}
	}
	
	/**
	 * 
	 * @param val
	 * @return
	 */
	private void view(Movie val) {
		val.setViewed(true);
		Date date = val.startToSee(new Date());
		for(int i = 0; i< 100_000; i++) {
			System.out.println("......");
		}
		val.stopToSee(date, new Date());
		System.out.println("Viste           : " + val.getTitle());
		System.out.println("Duraste viendola: " + val.getTimeViewed());
	}
	
	@Override
	public String reportViews() {
		StringBuffer report = new StringBuffer();
		for (Movie movie : list) {
			if(movie.isViewed())
				report.append(movie.getDataMin()).append("\n");
		}
		return report.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	private List<Movie> makeList() {
		if(list == null || list.isEmpty()) {
			list = new ArrayList<>();
			list.add(new Movie("Movie 1", "Genre 1", "Creator 1", 120, 1027));
			list.add(new Movie("Movie 2", "Genre 2", "Creator 2", 120, 1027));
			list.add(new Movie("Movie 3", "Genre 3", "Creator 3", 120, 1027));
			list.add(new Movie("Movie 4", "Genre 4", "Creator 4", 120, 1027));
			list.add(new Movie("Movie 5", "Genre 5", "Creator 5", 120, 1027));
		}
		return list;
	}

}
