package co.com.jimmcruz.amazon;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import co.com.jimmcruz.amazon.model.Chapter;
import co.com.jimmcruz.amazon.model.Serie;

/**
 * @author jimmcruz
 *
 */
public class SeriesFunction extends AbstractFunction {
	
	private List<Serie> list;

	/**
	 * 
	 */
	protected SeriesFunction(Scanner scanner) {
		super("Series", scanner);
	}

	@Override
	public void show() {
		showSubTitle();
		while (true) {
			for (Serie val : makeSeries()) {
				val.showDataMin();
			}
			System.out.println("0. Regresar al menú anterior");
			String responsest = leerOpcion();
			if (isNumeric(responsest)) {
				int response = Integer.parseInt(responsest);
				if (response == 0)
					return;
				selectSerie(response);
			} else {
				System.out.println("Seleccione una opción valida");
			}
		}
	}

	/**
	 * 
	 * @param val
	 */
	private void selectSerie(int pos) {
		if(pos <= list.size()) {
			view(list.get(pos - 1));
		} else {
			System.out.println("Seleccione una opción valida"); 
		}
	}
	
	/**
	 * 
	 * @param val
	 */
	private void view(Serie val) {
		System.out.println("Selecciono la serie " + val.getTitle());
		AmazonFactory.getChapter(val, getScanner());
	}
	/**
	 * 
	 * @return
	 */
	private List<Serie> makeSeries() {
		if(list == null || list.isEmpty()) {
			list = new ArrayList<>();
			list.add(new Serie("Serie 1", "Genero 1", "Creator 1", 120, getChapterList()));
			list.add(new Serie("Serie 2", "Genero 2", "Creator 2", 120, getChapterList()));
			list.add(new Serie("Serie 3", "Genero 3", "Creator 3", 120, getChapterList()));
			list.add(new Serie("Serie 4", "Genero 4", "Creator 4", 120, getChapterList()));
			list.add(new Serie("Serie 5", "Genero 5", "Creator 5", 120, getChapterList()));
			list.add(new Serie("Serie 6", "Genero 6", "Creator 6", 120, getChapterList()));
		}
		return list;
	}
	
	/**
	 * 
	 * @return
	 */
	private List<Chapter> getChapterList() {
		List<Chapter> list = new ArrayList<>();
		list.add(new Chapter("cahpter 1", 120, 2017, 1));
		list.add(new Chapter("cahpter 2", 120, 2017, 1));
		list.add(new Chapter("cahpter 3", 120, 2017, 1));
		list.add(new Chapter("cahpter 4", 120, 2017, 1));
		list.add(new Chapter("cahpter 5", 120, 2017, 1));
		return list;
	}

	@Override
	public String reportViews() {
		StringBuffer report = new StringBuffer();
		for (Serie serie : list) {
			for(Chapter chapter : serie.getChapters()) {
				if(chapter.isViewed()) {
					report.append(serie.getTitle()).append(" - ");
					report.append(chapter.getDataMin()).append("\n");
				}
			}
		}
		return report.toString();
	}

}
