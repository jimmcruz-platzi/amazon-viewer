package co.com.jimmcruz.amazon;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import co.com.jimmcruz.amazon.model.Chapter;
import co.com.jimmcruz.amazon.model.Serie;

/**
 * @author jimmcruz
 *
 */
public class ChapterFunction extends AbstractFunction {

	/**
	 * 
	 */
	protected ChapterFunction(Scanner scanner) {
		super("Chapters", scanner);
	}

	@Override
	public void show() {
		showSubTitle();
	}
	
	/**
	 * 
	 * @param serie
	 */
	public void show(Serie serie) {
		while (true) {
			for (Chapter val : serie.getChapters()) {
				val.showDataMin();
			}
			System.out.println("0. Regresar al menú anterior");
			String responsest = leerOpcion();
			if (isNumeric(responsest)) {
				int response = Integer.parseInt(responsest);
				if (response == 0)
					return;
				view(response, serie.getChapters());
			} else {
				System.out.println("Seleccione una opción valida");
			}
		}
	}
	
	/**
	 * 
	 * @param movie
	 */
	private void view(int pos, List<Chapter> list) {
		if (pos <= list.size()) {
			view(list.get(pos - 1));
		} else {
			System.out.println("Seleccione una opción valida");
		}
	}
	
	/**
	 * 
	 * @param val
	 */
	private void view(Chapter val) {
		val.setViewed(true);
		Date date = val.startToSee(new Date());
		for (int i = 0; i < 100_000; i++) {
			System.out.println("......");
		}
		val.stopToSee(date, new Date());
		System.out.println("Viste           : " + val.getTitle());
		System.out.println("Dureste viendola: " + val.getTimeViewed());
	}

	@Override
	public String reportViews() {
		return "";
	}

}
