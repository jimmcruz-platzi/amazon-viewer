package co.com.jimmcruz.amazon;

import java.util.Scanner;

import co.com.jimmcruz.amazon.model.Serie;

/**
 * @author jimmcruz
 *
 */
public final class AmazonFactory {

	private static MenuFunction menuFunction;
	private static MovieFunction movieFunction;
	private static SeriesFunction seriesFunction;

	/**
	 * 
	 */
	private AmazonFactory() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public static AbstractFunction getMenu(Scanner scanner) {
		if (menuFunction == null)
			menuFunction = new MenuFunction(scanner);
		menuFunction.show();
		return menuFunction;
	}

	/**
	 * 
	 * @return
	 */
	public static AbstractFunction getMovies(Scanner scanner) {
		if(movieFunction==null)
			movieFunction = new MovieFunction(scanner);
		movieFunction.show();
		return movieFunction;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getMoviesReport() {
		return movieFunction.reportViews();
	}

	/**
	 * 
	 * @return
	 */
	public static AbstractFunction getSeries(Scanner scanner) {
		if(seriesFunction==null)
			seriesFunction =new SeriesFunction(scanner); 
		seriesFunction.show();
		return seriesFunction;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getSeriesReport() {
		return seriesFunction.reportViews();
	}

	/**
	 * 
	 * @return
	 */
	public static AbstractFunction getChapter(Serie serie, Scanner scanner) {
		ChapterFunction fun = new ChapterFunction(scanner);
		fun.show(serie);
		return fun;
	}

	/**
	 * 
	 * @return
	 */
	public static AbstractFunction getBooks(Scanner scanner) {
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public static AbstractFunction getMagazines(Scanner scanner) {
		return null;
	}
	
	/**
	 * 
	 * @return
	 */
	public static void report() {
		new ReportFunction();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AmazonFactory []");
		return builder.toString();
	}

}
