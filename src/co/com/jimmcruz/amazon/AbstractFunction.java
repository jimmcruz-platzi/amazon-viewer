package co.com.jimmcruz.amazon;

import java.util.Scanner;

/**
 * @author jimmcruz
 *
 */
public abstract class AbstractFunction {

	private String title = "";
	private Scanner scanner;

	/**
	 * 
	 */
	protected AbstractFunction(String title, Scanner scanner) {
		super();
		this.title = title;
		this.scanner = scanner;
	}

	/**
	 * 
	 */
	public abstract void show();
	
	/**
	 * 
	 */
	public abstract String reportViews();

	/**
	 * 
	 * @param title
	 */
	protected void showSubTitle() {
		System.out.println();
		System.out.println(title + ":");
		System.out.println();
	}

	/**
	 * 
	 * @return
	 */
	protected String leerOpcion() {
		return scanner.nextLine();
	}

	/**
	 * 
	 * @param cadena
	 * @return
	 */
	protected boolean isNumeric(String st) {
		if (st == null || st.isEmpty())
			return false;

		for (int i = 0; i < st.length(); i++) {
			if (!Character.isDigit(st.charAt(i)))
				return false;
		}
		return true;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the scanner
	 */
	public Scanner getScanner() {
		return scanner;
	}

	/**
	 * @param scanner the scanner to set
	 */
	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AbstractFunction [title=");
		builder.append(title);
		builder.append(", scanner=");
		builder.append(scanner);
		builder.append("]");
		return builder.toString();
	}

}
