/**
 * 
 */
package co.com.jimmcruz.amazon.model;

/**
 * @author jimmcruz
 *
 */
public class Film {

	private int id;
	private String title;
	private String genre;
	private String creator;
	private int duration;
	private int year;
	private boolean viewed;

	/**
	 * 
	 */
	public Film() {
		super();
	}

	/**
	 * @param title
	 * @param genre
	 * @param creator
	 * @param duration
	 */
	public Film(String title, String genre, String creator, int duration) {
		super();
		this.title = title;
		this.genre = genre;
		this.creator = creator;
		this.duration = duration;
	}

	/**
	 * 
	 * @return
	 */
	public String stView() {
		return isViewed() ? "Si" : "No";
	}

	/**
	 * 
	 */
	public void see() {
		// TODO implementar
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the viewed
	 */
	public boolean isViewed() {
		return viewed;
	}

	/**
	 * @param viewed the viewed to set
	 */
	public void setViewed(boolean viewed) {
		this.viewed = viewed;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Film [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", genre=");
		builder.append(genre);
		builder.append(", creator=");
		builder.append(creator);
		builder.append(", duration=");
		builder.append(duration);
		builder.append(", year=");
		builder.append(year);
		builder.append(", viewed=");
		builder.append(viewed);
		builder.append("]");
		return builder.toString();
	}

}
