package co.com.jimmcruz.amazon.model;

/**
 * @author jimmcruz
 *
 */
public class Chapter extends Movie {

	private int id;
	private int sessionNumber;

	/**
	 * 
	 */
	public Chapter() {
		super();
	}

	/**
	 * @param title
	 * @param duration
	 * @param year
	 */
	public Chapter(String title, int duration, int year, int sessionNumber) {
		super();
		this.setTitle(title);
		this.setDuration(duration);
		this.setYear(year);
		this.sessionNumber = sessionNumber;
	}

	/**
	 * @param id
	 * @param title
	 * @param duration
	 * @param year
	 * @param viewed
	 * @param timeViewed
	 * @param sessionNumber
	 */
	public Chapter(int id, String title, int duration, short year, boolean viewed, int timeViewed, int sessionNumber) {
		super();
		this.id = id;
		this.setTitle(title);
		this.setDuration(duration);
		this.setYear(year);
		this.setViewed(viewed);
		this.setTimeViewed(timeViewed);
		this.sessionNumber = sessionNumber;
	}

	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return the sessionNumber
	 */
	public int getSessionNumber() {
		return sessionNumber;
	}

	/**
	 * @param sessionNumber the sessionNumber to set
	 */
	public void setSessionNumber(int sessionNumber) {
		this.sessionNumber = sessionNumber;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Chapter [id=");
		builder.append(id);
		builder.append(", sessionNumber=");
		builder.append(sessionNumber);
		builder.append("]");
		return builder.toString();
	}

}
