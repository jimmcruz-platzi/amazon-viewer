package co.com.jimmcruz.amazon.model;

import java.util.Date;
import java.util.List;

/**
 * @author jimmcruz
 *
 */
public class Publication {

	private int id;
	private String title;
	private Date editionDate;
	private String editorial;
	private List<String> autores;
	
	/**
	 * 
	 */
	public Publication() {
		super();
	}

	/**
	 * @param id
	 * @param title
	 * @param editionDate
	 * @param editorial
	 * @param autores
	 */
	public Publication(int id, String title, Date editionDate, String editorial, List<String> autores) {
		super();
		this.id = id;
		this.title = title;
		this.editionDate = editionDate;
		this.editorial = editorial;
		this.autores = autores;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the editionDate
	 */
	public Date getEditionDate() {
		return editionDate;
	}

	/**
	 * @param editionDate the editionDate to set
	 */
	public void setEditionDate(Date editionDate) {
		this.editionDate = editionDate;
	}

	/**
	 * @return the editorial
	 */
	public String getEditorial() {
		return editorial;
	}

	/**
	 * @param editorial the editorial to set
	 */
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	/**
	 * @return the autores
	 */
	public List<String> getAutores() {
		return autores;
	}

	/**
	 * @param autores the autores to set
	 */
	public void setAutores(List<String> autores) {
		this.autores = autores;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Publication [id=").append(id).append(", title=").append(title).append(", editionDate=")
				.append(editionDate).append(", editorial=").append(editorial).append(", autores=").append(autores)
				.append("]");
		return builder.toString();
	}

}
