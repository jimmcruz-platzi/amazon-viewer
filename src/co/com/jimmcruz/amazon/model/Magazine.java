package co.com.jimmcruz.amazon.model;

import java.util.Date;
import java.util.List;

/**
 * @author jimmcruz
 *
 */
public class Magazine extends Publication {

	private int id;

	/**
	 * 
	 */
	public Magazine() {
		super();
	}

	/**
	 * @param title
	 * @param editionDate
	 * @param editorial
	 */
	public Magazine(String title, Date editionDate, String editorial) {
		super();
		this.setTitle(title);
		this.setEditionDate(editionDate);
		this.setEditorial(editorial);
	}

	/**
	 * @param id
	 * @param title
	 * @param editionDate
	 * @param editorial
	 * @param autores
	 */
	public Magazine(int id, String title, Date editionDate, String editorial, List<String> autores) {
		super();
		this.id = id;
		this.setTitle(title);
		this.setEditionDate(editionDate);
		this.setEditorial(editorial);
		this.setAutores(autores);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Magazine [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

}
