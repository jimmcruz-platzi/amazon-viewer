package co.com.jimmcruz.amazon.model;

import java.util.Date;

/**
 * @author jimmcruz
 *
 */
public interface IVisualizable {

	/**
	 * Método que recive la fecha de inicio de la viusalización
	 * 
	 * @param dateI
	 * @return
	 */
	Date startToSee(Date dateI);

	/**
	 * Método que registra la finalización de la visualización
	 * 
	 * @param dateI
	 * @param dateF
	 */
	void stopToSee(Date dateI, Date dateF);

}
