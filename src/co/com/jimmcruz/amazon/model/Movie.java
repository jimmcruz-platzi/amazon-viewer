package co.com.jimmcruz.amazon.model;

import java.util.Date;

/**
 * @author jimmcruz
 *
 */
public class Movie extends Film implements IVisualizable {

	private int id;
	private int timeViewed;

	/**
	 * 
	 */
	public Movie() {
		super();
	}
	
	/**
	 * 
	 */
	public Movie(String title, String genre, String creator, int duration, int year) {
		super(title, genre, creator, duration);
		this.setYear(year);
	}	

	/**
	 * @param id
	 * @param title
	 * @param genre
	 * @param creator
	 * @param duration
	 * @param year
	 * @param viewed
	 * @param timeViewed
	 */
	public Movie(int id, String title, String genre, String creator, int duration, short year, boolean viewed,
			int timeViewed) {
		super(title, genre, creator, duration);
		this.id = id;
		this.setYear(year);
		this.setViewed(viewed);
		this.timeViewed = timeViewed;
	}

	/**
	 * Imprime los datos de la película
	 */
	public void showData() {
		System.out.println("Title:" + getTitle());
		System.out.println("Genre:" + getGenre());
		System.out.println("Year:" + getYear());
	}
	
	/**
	 * Imprime los datos de la película
	 */
	public void showDataMin() {
		System.out.println(getId() + ". " + getTitle() + " - Visto: " + stView());
	}
	
	/**
	 * Imprime los datos de la película
	 */
	public String getDataMin() {
		return getId() + ". " + getTitle() + " - Visto: " + stView();
	}
	
	@Override
	public Date startToSee(Date dateI) {
		return dateI;
	}

	@Override
	public void stopToSee(Date dateI, Date dateF) {
		if(dateF.getTime() > dateI.getTime())
			setTimeViewed((int)(dateF.getTime() - dateI.getTime()));
		 else
			setTimeViewed(0);	
	}
	
	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return the timeViewed
	 */
	public int getTimeViewed() {
		return timeViewed;
	}

	/**
	 * @param timeViewed the timeViewed to set
	 */
	public void setTimeViewed(int timeViewed) {
		this.timeViewed = timeViewed;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Movie [id=").append(id).append(", timeViewed=").append(timeViewed).append("]");
		return builder.toString();
	}

}
