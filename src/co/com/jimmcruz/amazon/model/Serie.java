package co.com.jimmcruz.amazon.model;

import java.util.List;

/**
 * @author jimmcruz
 *
 */
public class Serie extends Film {

	private int id;
	private int sessionQuantity;
	private List<Chapter> chapters;

	/**
	 * 
	 */
	public Serie() {
		super();
	}

	/**
	 * 
	 * @param title
	 * @param genre
	 * @param creator
	 * @param duration
	 */
	public Serie(String title, String genre, String creator, int duration) {
		super(title, genre, creator, duration);
	}

	/**
	 * 
	 * @param title
	 * @param genre
	 * @param creator
	 * @param duration
	 * @param chapters
	 */
	public Serie(String title, String genre, String creator, int duration, List<Chapter> chapters) {
		super(title, genre, creator, duration);
		this.chapters = chapters;
	}

	/**
	 * @param id
	 * @param title
	 * @param genre
	 * @param creator
	 * @param duration
	 * @param year
	 * @param viewed
	 * @param sessionQuantity
	 * @param chapters
	 */
	public Serie(int id, String title, String genre, String creator, int duration, int year, boolean viewed,
			int sessionQuantity, List<Chapter> chapters) {
		super(title, genre, creator, duration);
		this.id = id;
		this.setYear(year);
		this.setViewed(viewed);
		this.sessionQuantity = sessionQuantity;
		this.chapters = chapters;
	}

	/**
	 * Imprime los datos de la película
	 */
	public void showData() {
		System.out.println("Title:" + getTitle());
		System.out.println("Genre:" + getGenre());
		System.out.println("Year:" + getYear());
	}

	/**
	 * Imprime los datos de la película
	 */
	public void showDataMin() {
		System.out.println(getId() + ". " + getTitle());
	}

	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return the sessionQuantity
	 */
	public int getSessionQuantity() {
		return sessionQuantity;
	}

	/**
	 * @param sessionQuantity the sessionQuantity to set
	 */
	public void setSessionQuantity(int sessionQuantity) {
		this.sessionQuantity = sessionQuantity;
	}

	/**
	 * @return the chapters
	 */
	public List<Chapter> getChapters() {
		return chapters;
	}

	/**
	 * @param chapters the chapters to set
	 */
	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Serie [id=");
		builder.append(id);
		builder.append(", sessionQuantity=");
		builder.append(sessionQuantity);
		builder.append(", chapters=");
		builder.append(chapters);
		builder.append("]");
		return builder.toString();
	}

}
