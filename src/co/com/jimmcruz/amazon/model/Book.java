package co.com.jimmcruz.amazon.model;

import java.util.Date;
import java.util.List;

/**
 * @author jimmcruz
 *
 */
public class Book extends Publication implements IVisualizable {

	private int id;
	private String isbn;
	private boolean readed;
	private int timeReaded;

	/**
	 * 
	 */
	public Book() {
		super();
	}

	/**
	 * @param title
	 * @param editionDate
	 * @param editorial
	 * @param isbn
	 */
	public Book(String title, Date editionDate, String editorial, String isbn) {
		super();
		this.setTitle(title);
		this.setEditionDate(editionDate);
		this.setEditorial(editorial);
		this.isbn = isbn;
	}

	/**
	 * @param id
	 * @param title
	 * @param editionDate
	 * @param editorial
	 * @param autores
	 * @param isbn
	 * @param readed
	 * @param timeReaded
	 */
	public Book(int id, String title, Date editionDate, String editorial, List<String> autores, String isbn,
			boolean readed, int timeReaded) {
		super();
		this.id = id;
		this.setTitle(title);
		this.setEditionDate(editionDate);
		this.setEditorial(editorial);
		this.setAutores(autores);
		this.isbn = isbn;
		this.readed = readed;
		this.timeReaded = timeReaded;
	}
	
	@Override
	public Date startToSee(Date dateI) {
		return dateI;
	}

	@Override
	public void stopToSee(Date dateI, Date dateF) {
		if(dateF.getTime() >  dateI.getTime())
			setTimeReaded((int)(dateF.getTime() -  dateI.getTime())/1000);
		 else
			setTimeReaded(0);	
	}

	/**
	 * 
	 */
	public void read() {
		// TODO implementar
	}

	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * @return the readed
	 */
	public boolean isReaded() {
		return readed;
	}

	/**
	 * @param readed the readed to set
	 */
	public void setReaded(boolean readed) {
		this.readed = readed;
	}

	/**
	 * @return the timeReaded
	 */
	public int getTimeReaded() {
		return timeReaded;
	}

	/**
	 * @param timeReaded the timeReaded to set
	 */
	public void setTimeReaded(int timeReaded) {
		this.timeReaded = timeReaded;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Book [id=").append(id).append(", isbn=").append(isbn).append(", readed=").append(readed)
				.append(", timeReaded=").append(timeReaded).append("]");
		return builder.toString();
	}

}
