/**
 * 
 */
package co.com.jimmcruz.amazon;

import java.util.Scanner;

/**
 * @author jimmcruz
 *
 */
public class MenuFunction extends AbstractFunction {
	
	/**
	 * 
	 */
	public MenuFunction(Scanner scanner) {
		super("Amazon Viewer", scanner);
	}
	
	@Override
	public void show() {
		showSubTitle();
		boolean exit = false;
		while (!exit) {
			System.out.println("");
			System.out.println("Selecciona el número de opción deseada:");
			System.out.println("1. Movies");
			System.out.println("2. Series");
			System.out.println("3. Books");
			System.out.println("4. Magazines");
			System.out.println("5. Report");
			System.out.println("0. Exit");
			
			String responsest = leerOpcion();
			if(isNumeric(responsest)) {
				int response = Integer.parseInt(responsest);
				switch (response) {
				case 0:
					exit = true;
					System.out.println("Saliste de " + getTitle());
					break;
				case 1:
					// movies
					AmazonFactory.getMovies(getScanner());
					break;
				case 2:
					// series
					AmazonFactory.getSeries(getScanner());
					break;
				case 3:
					// books
					AmazonFactory.getBooks(getScanner());
					break;
				case 4:
					// magazines
					AmazonFactory.getMagazines(getScanner());
					break;
				case 5:
					// magazines
					AmazonFactory.report();
					break;
				default:
					System.out.println();
					System.out.println("Selecciones una opción:");
					System.out.println();
					break;
				}
			} else {
				System.out.println("Seleccione una opción valida");
			}
		}
	}

	@Override
	public String reportViews() {
		return "";
	}

}
