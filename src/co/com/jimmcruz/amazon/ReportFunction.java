package co.com.jimmcruz.amazon;

import co.com.jimmcruz.amazon.report.Report;

/**
 * 
 * @author jimmcruz
 *
 */
public class ReportFunction {

	/**
	 * 
	 * @param scanner
	 */
	public ReportFunction() {
		Report report = new Report("Reporte.txt");
		report.setTitle("Vistos");
		StringBuffer content = new StringBuffer();
		content.append("Movies views:").append("\n");
		content.append(AmazonFactory.getMoviesReport());
		content.append("Series views:").append("\n");
		content.append(AmazonFactory.getSeriesReport());
		report.setContent(content.toString());
		report.makeReport();
	}
}
